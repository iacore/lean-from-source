import * as fs from "https://deno.land/std@0.178.0/fs/mod.ts"
import * as path from "https://deno.land/std@0.178.0/path/mod.ts"
import { pooledMap } from "https://deno.land/std@0.178.0/async/mod.ts"
import {
  createLogger,
  consoleSink,
} from "https://deno.land/x/deno_structured_logging@0.4.2/mod.ts"
import { crypto } from "https://deno.land/std@0.178.0/crypto/mod.ts";
import { encode64 } from "https://deno.land/x/base64to@v1.0.0/mod.ts";

import $ from "https://deno.land/x/dax/mod.ts"

import { asyncIterToArray, requirePrograms, randomid, assert, fail } from "./util.ts"

/* load .env */
import "https://deno.land/std@0.178.0/dotenv/load.ts"
/* config */
export const LEAN_REPO =
  Deno.env.get("LEAN_REPO") ?? "/home/user/computing/lang/lean4"
export const LEAN_SRC = path.join(LEAN_REPO, "src")
export const OUT_DIR = Deno.env.get("OUT_DIR") ?? "build"
export const PATH_OBJLEANINIT = path.join(OUT_DIR, "leaninit.o")
export const PATH_OBJLEANRUNTIME = path.join(OUT_DIR, "leanruntime.o")
export const TARGET_TRIPLE = Deno.env.get("TARGET") ?? "native"

const INCLUDE_DIRS = [".", path.join(LEAN_SRC, "include"), LEAN_SRC]
const CFLAGS = [...INCLUDE_DIRS.map((o) => "-I" + o)]

export const logger = createLogger({ outputFormat: "{message}" })
async function compile_libInit() {
  await requirePrograms(["lean", "zig"])

  async function* files() {
    yield path.join(LEAN_REPO, "src/Init.lean")

    for await (const file of fs.expandGlob("src/Init/**/*.lean", {
      root: LEAN_REPO,
    })) {
      yield file.path
    }
  }

  async function deterministicFileName(leanFile: string): Promise<string> {
    const content = await Deno.readFile(leanFile)
    const buf = crypto.subtle.digestSync('SHA-256', content)
    const digest = encode64(new Uint8Array(buf), '+', '-', '')
    return path.join(OUT_DIR, digest + ".c")
  }

  //   lean", ["--root", lean_src, "-c", outfile, path]
  async function compileLean(leanFile: string): Promise<string> {
    const cFile = await deterministicFileName(leanFile)
    if (await fs.exists(cFile)) return cFile

    logger.info("built {out} {in}", cFile, leanFile)
    await $`lean --root ${LEAN_SRC} -c ${cFile} ${leanFile}`

    return cFile
  }

  // todo: limit to #cpus
  await fs.ensureDir(OUT_DIR)
  const cFiles = await asyncIterToArray(pooledMap(8, files(), compileLean))

  await $`zig cc --target=${TARGET_TRIPLE} ${CFLAGS} -c -o ${PATH_OBJLEANINIT} ${cFiles}`
  logger.info("built {out}", PATH_OBJLEANINIT)
}

async function compile_runtime() {
  await requirePrograms(["lean"])

  const cFiles = (
    await asyncIterToArray(
      fs.expandGlob("src/runtime/*.cpp", {
        root: LEAN_REPO,
      })
    )
  ).map((o) => o.path)
  console.log(cFiles)
  await $`zig c++ --target=${TARGET_TRIPLE} --std=c++20 ${CFLAGS} -c -o ${PATH_OBJLEANRUNTIME} ${cFiles}`
  logger.info("built {out}", PATH_OBJLEANRUNTIME)
}

import yargs from "https://deno.land/x/yargs@v17.4.0-deno/deno.ts"
import type { Arguments } from "https://deno.land/x/yargs@v17.4.0-deno/deno-types.ts"
import type { YargsInstance } from "https://deno.land/x/yargs@v17.4.0-deno/build/lib/yargs-factory.js";

if (import.meta.main) {
  logger.addSink(consoleSink())

  logger.debug("Config:")
  for (const [k, v] of Object.entries({
    LEAN_REPO,
    LEAN_SRC,
    OUT_DIR, 
    PATH_OBJLEANINIT,
    PATH_OBJLEANRUNTIME,
    TARGET_TRIPLE,
    CFLAGS: CFLAGS.join(" "),
  })) {
    // todo: print table
    logger.debug(`{k}\t{v}`, k, v)
  }

  try {
    const statLeanRepo = await Deno.lstat(LEAN_REPO)
    assert(statLeanRepo.isDirectory, 'LEAN_REPO is not a directory')
  } catch (e) {
    if (e instanceof Deno.errors.NotFound) {
      fail("Please specify a valid LEAN_REPO")
    }
    throw e
  }


  yargs(Deno.args)
    .usage("buildlib.ts - executable object builder for Lean using Zig")
    .command(
      "init",
      `build object file for Init`,
      (yargs: YargsInstance) => {
        // return yargs
      },
      async (argv: Arguments) => {
        await compile_libInit()
      }
    )
    .command(
      "runtime",
      `build object file for Lean's runtime`,
      (yargs: YargsInstance) => {
        // return yargs
      },
      async (argv: Arguments) => {
        await compile_runtime()
      }
    )
    .strictCommands()
    .demandCommand(1)
    .parse()
}
