#!/usr/bin/env -S nim r --verbosity:0
import std/[os,sequtils,sugar]

import util
import compile_leanshared

if isMainModule:
  let TARGET_TRIPLE =
    if os.existsEnv("TARGET_TRIPLE"):
      os.getEnv("TARGET_TRIPLE")
    else:
      "native"
  let lean_repo = getLeanRepo()

  let I = @[".", lean_repo / "src/include", lean_repo / "src"]

  let include_args = I.map(it => "-I" & it)

  const MY_O = "build/my.o"
  const LIB_O = "build/lib.o"

  echo exec("zig", @["cc", "--target", TARGET_TRIPLE, "-c", "-o", MY_O] & include_args & walkFiles("build/*.c").toSeq)
  echo exec("zig", @["c++", "--target", TARGET_TRIPLE, "--std=c++20", "-c", "-o", LIB_O] & include_args & walkFiles(lean_repo / "src/runtime/*.cpp").toSeq)
  echo exec("zig", @["c++", "--target", TARGET_TRIPLE, "-o", "EmptyProgram", MY_O, LIB_O])
