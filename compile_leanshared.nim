#!/usr/bin/env -S nim r --verbosity:0
import std/[os,osproc,re,strutils,strformat,sets,sequtils,strtabs,streams,sugar]

import util

let re_olean = re".*/lib/lean/(.*)\.olean"
doAssert os.existsEnv("LEAN_REPO"), "Please set repo path of lean4 as LEAN_REPO"

const build_dir = "build"

proc getLeanFromOlean(lean_src, path: string): string =
  var matches: array[1, string]
  doAssert path.match(re_olean, matches), &"{path.repr}"
  lean_src / (matches[0] & ".lean")

proc compileLeanFile(lean_src: string, i: int, path: string) =
  debugEcho &"Compiling {path}"
  discard existsOrCreateDir(build_dir)
  let outfile = build_dir / &"{i}.c"
  if path.startsWith(lean_src):
    discard exec("lean", ["--root", lean_src, "-c", outfile, path])
  else:
    discard exec("lean", ["-c", outfile, path])

proc getLeanRepo*: string =
  os.getEnv("LEAN_REPO").strip(leading=false, chars={'/'})

proc main =
  let lean_repo = getLeanRepo()
  let lean_src = lean_repo / "src"
  let pwd = getCurrentDir()

  var allfiles: HashSet[string]
  var unvisited = @["EmptyProgram.lean"]

  while unvisited.len > 0:
    let leanfilename = unvisited.pop
    if leanfilename in allfiles: continue
    allfiles.incl leanfilename
    let outp = exec("lean", ["--deps", leanfilename])
    let newfiles = outp.splitLines.filter(it => it.len != 0).map(it => getLeanFromOlean(lean_src, it))
    unvisited &= newfiles

  for i, leanfilename in allfiles.toSeq:
    compileLeanFile(lean_src, i, leanfilename)

if isMainModule: main()
