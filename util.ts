import { assert, fail } from "https://deno.land/std/testing/asserts.ts";
import $ from "https://deno.land/x/dax/mod.ts"
import { customAlphabet } from "https://deno.land/x/nanoid/customAlphabet.ts"
export const randomid = customAlphabet("1234567890abcdef", 7)

export async function asyncIterToArray<T>(asyncIterator: AsyncIterable<T>): Promise<Array<T>> {
  const arr = []
  for await (const i of asyncIterator) arr.push(i)
  return arr
}

/** Check shell programs are installed */
export async function requirePrograms(programs: string[]) {
  for (const p of programs) {
    assert(await $.commandExists(p), "Program not found: " + p)
  }
}

// todo: use assert with better UX
export { assert, fail }
