## Usage

1. Install lean, zig, deno
2. Clone the lean4 git repo
3. Run the following

```
export LEAN_REPO=path/to/lean4
export TARGET=native-linux-musl # if you want to build with musl libc
deno run -A buildlib.ts init
deno run -A buildlib.ts runtime
```

## Usage (old)

1. Install nim, lean, zig
2. Clone the lean4 git repo
3. Run the following

```
export LEAN_REPO=path/to/lean4
./compile_leanshared.nim
./compile_program.nim
```
