import std/[os,osproc,re,strutils,strformat,sets,sequtils,strtabs,streams,sugar]

proc execProcessAssert*(command: string, workingDir: string = "",
    args: openArray[string] = [], env: StringTableRef = nil,
    options: set[ProcessOption] = {poStdErrToStdOut, poUsePath,
        poEvalCommand}):
  string =

  var p = startProcess(command, workingDir = workingDir, args = args,
      env = env, options = options)
  var outp = outputStream(p)
  result = ""
  var line = newStringOfCap(120)
  # consider `p.lines(keepNewLines=true)` to circumvent `running` busy-wait
  while true:
    if outp.readLine(line):
      result.add(line)
      result.add("\n")
    elif not running(p): break
  let code = peekExitCode(p)
  doAssert code >= 0
  doAssert code == 0, &"{command} {args} exited with {code}; output: {result}"
  close(p)

template exec*(cmd: untyped, argss: untyped): untyped =
  execProcessAssert(cmd, args=argss, options={poUsePath})
